+++
title = "Join"
slug = "join"
thumbnail = "images/tn.png"
description = "join"
+++

Hackerspace.gr is open for everyone to utilize its space and tools.

Just visit the space and join its vibrant community.

## Become a member

Our awesome members are the people who make this happen with a small sustaining subscription.

Do you want to be one of them? Fill [this form](https://www.hackerspace.gr/membership.php)

![](../images/hsgr_wall.jpg)

