+++
title = "About hsgr"
slug = "about"
description = "about"
+++

## Vision

Hackerspace.gr [HSGR] is a place for creativity, collaboration, research, development and, of course, education. It is more than just a physical space: It is a dynamic community with ideas inspired by the Open Source philosophy.

## The Idea

Every day through internet we share data and perceptions, problems and solutions, from our home or work. So, why not do it face to face as well? The idea is derived from Free / Open-Source Software culture and from the desire to share our experiences and knowledge. We want to share our ideas, exactly like we share our code.
Values

## Open Source

The basic operetional principle of the Hackerspace is that we share projects, code, ideas that we produce inside that space, in collaboration with each other. This requires that all these are distributed freely with relevant license whether it's code, schematics, constructions, content.

## Non Profit

We operate in a non-profit manner and we mainly support like-minded projects and initiatives. Participation on all projects, events, but also in the day-to-day operation of the Hackerspace, is free and open to all.

## Code of Conduct

Be polite to everyone. Respect all people that you meet at the space, as well as the space itself. Everyone is welcome. Hackerspace is a safe place for all people, regardless of age, race, gender, appearance or linux distribution.

## Consensus

All of our decisions are the result of consensus among operators. Operators are responsible for the everyday needs of the Hackerspace. Decisions take place at the assembly meetings, which are open to everyone.
Do-ocracy

Nobody needs a permission to work on a project. Anyone that wants to organize an event or a workshop, that is aligned with Hackerspace principles and vision, doesn't need to wait for someone else to do it. She can take the initiative and and do it.

----
#### Location

A 120sq.m. space open almost 24/7. You can find us in Ampatiellou 11, 11144, Athens, 3 blocks south of Subway Station Ag. Eleytherios.

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=23.730007410049442%2C38.016157887560595%2C23.73251795768738%2C38.017687787684736&amp;layer=mapnik&amp;marker=38.01692284161464%2C23.731262683868408" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=38.01692&amp;mlon=23.73126#map=19/38.01692/23.73126">View Larger Map</a></small>