+++
title = "Contact"
slug = "contact"
thumbnail = "images/tn.png"
description = "contact"
+++

##### Want to get in touch, or ask anything about the space?

Feel free to reach us through:
- Phone +30 213 0210 437
- [Mailing List](https://lists.hackerspace.gr/listinfo/discuss)
- [Matrix](https://riot.im/app/#/room/#hsgr:matrix.org)